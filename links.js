(function(){

   var locserv = window.locserv || {};

   locserv.links = [
      {
         "title": "Confluence Space",
         "href": "https://confluence.dev.bbc.co.uk/display/locservices/Location+Services"
      }
   ];

   window.locserv = locserv;
}());